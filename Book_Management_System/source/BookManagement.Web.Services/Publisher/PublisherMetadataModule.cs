﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagement.Web.Services.Category;
using BookManagement.Web.Services.Models;
using Nancy.Metadata.Modules;
using Nancy.Swagger;
using Swagger.ObjectModel;

namespace BookManagement.Web.Services.Publisher
{
    public class PublisherMetadataModule : MetadataModule<PathItem>
    {
        public PublisherMetadataModule(ISwaggerModelCatalog modelCatalog)
        {
            modelCatalog.AddModels(typeof(PublisherResponse), typeof(PublisherResponse));

            Describe["GetPublishers"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("GetPublishers")
                        .Tag("Publisher")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        .Response(r =>
                            r.Schema<HealthResponse>(modelCatalog).Description("Here is the publisher status"))
                ));

            Describe["DeletePublisher"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("DeletePublisher")
                        .Tag("Publisher")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        .Parameter(new Parameter
                        {
                            Name = "id",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Path,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Publisher Id",
                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Publisher Deleted"))
                ));

            Describe["AddPublisher"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("AddPublisher")
                        .Tag("Publisher")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        //specify the parameters of this api   
                        .Parameter(new Parameter
                        {
                            Name = "PublisherModel",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Body,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Publisher to be added",
                            Default = new
                            {
                                publisherName = "Juta Publishers",
                            }

                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Publisher Added"))
                ));

            Describe["UpdatePublisher"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("UpdatePublisher")
                        .Tag("Publisher")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        //specify the parameters of this api   
                        .Parameter(new Parameter
                        {
                            Name = "publisherId",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Path,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Publisher Id To be updated"
                        })
                        .Parameter(new Parameter
                        {
                            Name = "PublisherModel",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Body,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Publisher to be updated",
                            Default = new
                            {
                                publisherName = "Zaful",
                            }

                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Publisher Updated"))
                ));
        }
    }
}
