﻿using BookManagement.Web.Services.Interfaces;
using BookManagement.Web.Services.Models;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;

namespace BookManagement.Web.Services.Publisher
{
    public class PublisherModule : NancyModule
    {
        private readonly IBookRepository _bookRepository;
        public PublisherModule(IBookRepository bookRepository) : base("/api/publisher")
        {
            _bookRepository = bookRepository;

            Get("/", _ => GetPublishers(), null, "GetPublishers");
            Post("/", _ => AddPublisher(), null, "AddPublisher");
            Delete("/{id}", parameters => DeletePublisher(parameters), null, "DeletePublisher");
            Put("/{publisherId}", parameters => UpdatePublisher(), null, "UpdatePublisher");
        }

        private Response GetPublishers()
        {
            return Response.AsJson(_bookRepository.GetPublishers());
        }

        private Negotiator AddPublisher()
        {
            var model = this.Bind<PublisherModel>();
            _bookRepository.AddPublisher(model);
            return Negotiate.WithModel(model)
                .WithStatusCode(HttpStatusCode.Created);
        }

        private HttpStatusCode DeletePublisher(dynamic parameters)
        {
            _bookRepository.DeletePublisher(parameters.Id);
            return HttpStatusCode.OK;
        }

        private Response UpdatePublisher()
        {
            var model = this.Bind<PublisherModel>();
            _bookRepository.UpdatePublisher(model);
            return Response.AsJson(model);
        }
                
        private static bool PublisherIsNotValid(PublisherModel publisher)
        {
            return string.IsNullOrWhiteSpace(publisher.PublisherName);
        }
    }
}
