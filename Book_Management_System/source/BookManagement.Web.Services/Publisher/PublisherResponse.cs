﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagement.Web.Services.Models;

namespace BookManagement.Web.Services.Publisher
{
    public class PublisherResponse
    {
        public IEnumerable<PublisherModel> Response { get; set; }
    }
}
