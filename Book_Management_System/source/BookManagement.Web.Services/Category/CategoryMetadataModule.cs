﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagement.Web.Services.Models;
using Nancy.Metadata.Modules;
using Nancy.Swagger;
using Swagger.ObjectModel;

namespace BookManagement.Web.Services.Category
{
    public class CategoryMetadataModule : MetadataModule<PathItem>
    {
        public CategoryMetadataModule(ISwaggerModelCatalog modelCatalog)
        {
            modelCatalog.AddModels(typeof(CategoryResponse), typeof(CategoryResponse));

            Describe["GetCategories"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("GetCategories")
                        .Tag("Category")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Here is the category status"))
                ));

            Describe["DeleteCategory"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("DeleteCategory")
                        .Tag("Category")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        .Parameter(new Parameter
                        {
                            Name = "id",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Path,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Category Id",
                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Category Deleted"))
                ));

            Describe["AddCategory"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("AddCategory")
                        .Tag("Category")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        //specify the parameters of this api   
                        .Parameter(new Parameter
                        {
                            Name = "CategoryModel",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Body,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Category to be added",
                            Default = new
                            {
                                categoryName = "Nature",
                            }

                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Category Added"))
                ));

            Describe["UpdateCategory"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("UpdateCategory")
                        .Tag("Category")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        //specify the parameters of this api   
                        .Parameter(new Parameter
                        {
                            Name = "categoryId",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Path,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Category Id To be updated"
                        })
                        .Parameter(new Parameter
                        {
                            Name = "CategoryModel",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Body,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Category to be updated",
                            Default = new
                            {   
                                categoryName = "Health",
                            }

                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Category Updated"))
                ));

        }
    }
}
