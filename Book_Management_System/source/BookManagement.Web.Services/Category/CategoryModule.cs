﻿using BookManagement.Web.Services.Interfaces;
using BookManagement.Web.Services.Models;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Response = Nancy.Response;

namespace BookManagement.Web.Services.Category
{
    public class CategoryModule : NancyModule
    {
        private readonly IBookRepository _bookRepository;
        public CategoryModule(IBookRepository bookRepository) : base("/api/category")
        {
            _bookRepository = bookRepository;

            Get("/", _ => GetCategories(), null, "GetCategories");
            Post("/", _ => AddCategory(), null, "AddCategory");
            Delete("/{id}", parameters => DeleteCategory(parameters), null, "DeleteCategory");
            Put("/{categoryId}", parameters => UpdateCategory(), null, "UpdateCategory");
        }

        private Response GetCategories()    
        {
            return Response.AsJson(_bookRepository.GetCategories());
        }

        private Negotiator AddCategory()
        {   
            var model = this.Bind<CategoryModel>();
            _bookRepository.AddCategory(model);
            return Negotiate.WithModel(model)
                .WithStatusCode(HttpStatusCode.Created);
        }

        private HttpStatusCode DeleteCategory(dynamic parameters)
        {
            _bookRepository.DeleteCategory(parameters.Id);
            return HttpStatusCode.OK;
        }

        private Response UpdateCategory()   
        {
            var model = this.Bind<CategoryModel>();
            _bookRepository.UpdateCategory(model);
            return Response.AsJson(model);
        }
                
        private static bool CategoryIsNotValid(CategoryModel category)
        {
            return string.IsNullOrWhiteSpace(category.CategoryName);
        }
    }
}
