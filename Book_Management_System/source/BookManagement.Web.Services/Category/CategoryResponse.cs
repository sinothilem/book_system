﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagement.Web.Services.Models;

namespace BookManagement.Web.Services.Category
{
    public class CategoryResponse
    {
        public IEnumerable<CategoryModel> Response { get; set; }
    }
}
