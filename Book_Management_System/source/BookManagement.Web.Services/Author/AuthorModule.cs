﻿using BookManagement.Web.Services.Interfaces;
using BookManagement.Web.Services.Models;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Response = Nancy.Response;

namespace BookManagement.Web.Services.Author
{
    public class AuthorModule : NancyModule
    {
        private readonly IBookRepository _bookRepository;
        public AuthorModule(IBookRepository bookRepository) : base("/api/author")
        {
            _bookRepository = bookRepository;

            Get("/", _ => GetAuthors(), null, "GetAuthors");
            Post("/", _ => AddAuthor(), null, "AddAuthor");
            Delete("/{id}", parameters => DeleteAuthor(parameters), null, "DeleteAuthor");
            Put("/{authorId}", parameters => UpdateAuthor(), null, "UpdateAuthor");
        }

        private Response GetAuthors()
        {
            return Response.AsJson(_bookRepository.GetAuthors());
        }

        private Negotiator AddAuthor()
        {
            var model = this.Bind<AuthorModel>();
            _bookRepository.AddAuthor(model);
            return Negotiate.WithModel(model)
                .WithStatusCode(HttpStatusCode.Created);
        }

        private HttpStatusCode DeleteAuthor(dynamic parameters)
        {
            _bookRepository.DeleteAuthor(parameters.Id);
            return HttpStatusCode.OK;
        }

        private Response UpdateAuthor()
        {
            var model = this.Bind<AuthorModel>();
            _bookRepository.UpdateAuthor(model);
            return Response.AsJson(model);
        }

        private static bool AuthorIsNotValid(AuthorModel author)
        {
            return string.IsNullOrWhiteSpace(author.AuthorName);
        }
    }
}
