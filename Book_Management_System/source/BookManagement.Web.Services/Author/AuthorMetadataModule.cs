﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagement.Web.Services.Book;
using BookManagement.Web.Services.Models;
using Nancy.Metadata.Modules;
using Nancy.Swagger;
using Swagger.ObjectModel;

namespace BookManagement.Web.Services.Author
{
    public class AuthorMetadataModule : MetadataModule<PathItem>
    {
        public AuthorMetadataModule(ISwaggerModelCatalog modelCatalog)
        {
            modelCatalog.AddModels(typeof(AuthorResponse), typeof(AuthorResponse));

            Describe["GetAuthors"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("GetAuthors")
                        .Tag("Author")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        .Response(r =>
                            r.Schema<HealthResponse>(modelCatalog).Description("Here is the author status"))
                ));

            Describe["DeleteAuthor"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("DeleteAuthor")
                        .Tag("Author")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        .Parameter(new Parameter
                        {
                            Name = "id",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Path,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Author Id",
                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Author Deleted"))
                ));

            Describe["AddAuthor"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("AddAuthor")
                        .Tag("Author")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        //specify the parameters of this api   
                        .Parameter(new Parameter
                        {
                            Name = "AuthorModel",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Body,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Author to be added",
                            Default = new
                            {
                                authorName = "Sinothile",
                            }

                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Author Added"))
                ));

            Describe["UpdateAuthor"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("UpdateAuthor")
                        .Tag("Author")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        //specify the parameters of this api   
                        .Parameter(new Parameter
                        {
                            Name = "authorId",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Path,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Author Id To be updated"
                        })
                        .Parameter(new Parameter
                        {
                            Name = "AuthorModel",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Body,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Author to be updated",
                            Default = new
                            {
                                authorName = "Sinothile",
                            }   

                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Author Updated"))
                ));
        }
    }
}
