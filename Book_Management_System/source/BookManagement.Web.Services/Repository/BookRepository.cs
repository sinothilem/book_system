﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using BookManagement.Web.Services.Interfaces;
using BookManagement.Web.Services.Models;
using Dapper;
using MySql.Data.MySqlClient;
using NHibernate.Util;

namespace BookManagement.Web.Services.Mappers
{
    public class BookRepository : IBookRepository
    {
        private MySqlConnection connectionString;

        public BookRepository()
        {
            connectionString =   new MySqlConnection(ConfigurationManager.ConnectionStrings["MySqlConnection"].ConnectionString);
        }
       
        public void Add(BookModel book)
        {
            using (var dbConnection = connectionString)
            {
                    string sQuery = $@"BEGIN;
                     INSERT INTO Book (Title,AuthorId,CategoryId,PublisherId, PublishDate) VALUES(@Title, {GetAuthorID(book)},{GetCategoryID(book)},{GetPublisherID(book)}, @PublishDate);
                     COMMIT";
                    dbConnection.Open();
                    dbConnection.Execute(sQuery, book);
               
            }
        }

        public IEnumerable<BookModel> GetAll()
        {
            using (var dbConnection = connectionString)
            {
                dbConnection.Open();
                return dbConnection.Query<BookModel>(@"select B.BookId, B.Title, C.CategoryName, A.AuthorName, P.PublisherName, B.PublishDate from Book B
                                                                         join Category C on B.CategoryId = C.CategoryId
                                                                         join Author A on B.AuthorId = A.AuthorId
                                                                         join Publisher P on B.PublisherId = P.PublisherId");
            }
        }

        public BookModel GetByID(int id)
        {
            using (var dbConnection = connectionString)
            {
                string sQuery = "SELECT * FROM Book"
                                + " WHERE BookId = @Id";
                dbConnection.Open();
                return dbConnection.Query<BookModel>(sQuery, new { Id = id }).FirstOrDefault();
            }
        }

        public void Delete(int id)
        {
            using (var dbConnection = connectionString)
            {
                string sQuery = "DELETE FROM Book"
                                + " WHERE BookId = @Id";
                dbConnection.Open();
                dbConnection.Execute(sQuery, new { Id = id });
            }
        }

        public void Update(BookModel book)
        {
            using (var dbConnection = connectionString)
            {
                string sQuery = $@"UPDATE Book 
                                SET Title = @Title, AuthorId= {GetAuthorID(book)}, CategoryId = {GetCategoryID(book)}, PublisherId ={GetPublisherID(book)},  PublishDate = @PublishDate
                                WHERE BookId = @BookId";
                dbConnection.Open();
                dbConnection.Query(sQuery, book);
            }
        }

        public IEnumerable<CategoryModel> GetCategories()   
        {
            using (var dbConnection = connectionString)
            {
                dbConnection.Open();
                return dbConnection.Query<CategoryModel>(@"select * from Category where IsDeleted = 0");
            }
        }

        public IEnumerable<AuthorModel> GetAuthors()
        {
            using (var dbConnection = connectionString)
            {
                dbConnection.Open();
                return dbConnection.Query<AuthorModel>(@"select * from Author where IsDeleted = 0");
            }
        }

        public IEnumerable<PublisherModel> GetPublishers()
        {
            using (var dbConnection = connectionString)
            {
                dbConnection.Open();
                return dbConnection.Query<PublisherModel>(@"select * from Publisher where IsDeleted = 0");
            }
        }

        private int GetAuthorID(BookModel book)
        {
            using (var dbConnection = connectionString)
            {
                dbConnection.Open();
                return dbConnection.Query<int>($@"select AuthorId from Author where AuthorName ='{book.AuthorName}' ").FirstOrDefault();
            }
        }

        private int GetCategoryID(BookModel book)
        {
            using (var dbConnection = connectionString)
            {
                dbConnection.Open();
                return dbConnection.Query<int>($@"select CategoryId from Category where CategoryName ='{book.CategoryName}' ").FirstOrDefault();
            }
        }

        private int GetPublisherID(BookModel book)
        {
            using (var dbConnection = connectionString)
            {
                dbConnection.Open();
                return dbConnection.Query<int>($@"select PublisherId from Publisher where PublisherName ='{book.PublisherName}' ").FirstOrDefault();
            }
        }

        public void AddAuthor(AuthorModel author)
        {
            using (var dbConnection = connectionString)
            {

                string sQuery = $@"
             INSERT INTO Author (AuthorName,DateOfBirth, IsDeleted) VALUES(@AuthorName,@DateOfBirth, 0);
               ";
                dbConnection.Open();
                dbConnection.Execute(sQuery, author);
            }
        }

        public void DeleteAuthor(int id)    
        {
            using (var dbConnection = connectionString)
            {
                string sQuery = "Update Author set IsDeleted = 1 WHERE AuthorId = @Id";
                dbConnection.Open();
                dbConnection.Execute(sQuery, new { Id = id });
            }
        }

        public void UpdateAuthor(AuthorModel author)
        {
            using (var dbConnection = connectionString)
            {
                string sQuery = $@"UPDATE Author 
                                SET AuthorName = @AuthorName, DateOfBirth = @DateOfBirth , IsDeleted = @IsDeleted
                                WHERE AuthorId = @AuthorId";
                dbConnection.Open();
                dbConnection.Query(sQuery, author);
            }
        }
            
        public void AddCategory(CategoryModel category)
        {
            using (var dbConnection = connectionString)
            {

                string sQuery = $@"
             INSERT INTO Category (CategoryName, IsDeleted) VALUES(@CategoryName, 0);
               ";
                dbConnection.Open();
                dbConnection.Execute(sQuery, category);
            }
        }

        public void DeleteCategory(int id)
        {
            using (var dbConnection = connectionString)
            {
                string sQuery = "Update Category set IsDeleted = 1 WHERE CategoryId = @Id";  
                dbConnection.Open();
                dbConnection.Execute(sQuery, new { Id = id });
            }
        }
            
        public void UpdateCategory(CategoryModel category)
        {
            using (var dbConnection = connectionString)
            {
                string sQuery = $@"UPDATE Category 
                                SET CategoryName = @CategoryName, IsDeleted = @IsDeleted
                                WHERE CategoryId = @CategoryId";    
                dbConnection.Open();
                dbConnection.Query(sQuery, category);
            }
        }
            
        public void AddPublisher(PublisherModel publisher)
        {
            using (var dbConnection = connectionString)
            {

                string sQuery = $@"
             INSERT INTO Publisher (PublisherName, IsDeleted) VALUES(@PublisherName, 0);
               ";
                dbConnection.Open();
                dbConnection.Execute(sQuery, publisher);
            }
        }

        public void DeletePublisher(int id) 
        {
            using (var dbConnection = connectionString)
            {
                string sQuery = "Update Publisher set IsDeleted = 1 WHERE PublisherId = @Id";
                dbConnection.Open();
                dbConnection.Execute(sQuery, new { Id = id });
            }
        }
            
        public void UpdatePublisher(PublisherModel publisher)
        {   
            using (var dbConnection = connectionString)
            {
                string sQuery = $@"UPDATE Publisher 
                                SET PublisherName = @PublisherName , IsDeleted = @IsDeleted
                                WHERE PublisherId = @PublisherId";
                dbConnection.Open();
                dbConnection.Query(sQuery, publisher);
            }
        }
    }
}
