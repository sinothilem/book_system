﻿using System;
using System.IO;
using System.Reflection;
using Nancy;
using Nancy.Responses;
using BookManagement.Web.Services.Health;
using Response = Nancy.Response;

namespace BookManagement.Web.Services.Health
{
    public class HealthModule : NancyModule
    {
        public HealthModule()
        {
            Get("/api/health", _ => HealthResponse(), null, "GetHealth");
        }

        private Response HealthResponse()
        {
            var result = new HealthResponse{ Response = "I am alive!" };
            return Response.AsJson(result);
        }
        
    }

}
