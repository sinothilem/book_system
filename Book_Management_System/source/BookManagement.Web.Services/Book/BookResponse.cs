﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookManagement.Web.Services.Models;

namespace BookManagement.Web.Services.Book
{
    public class BookResponse
    {
        public IEnumerable<BookModel> Response { get; set; }
    }
}
