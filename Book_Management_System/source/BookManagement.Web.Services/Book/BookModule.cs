﻿using System;
using BookManagement.Web.Services.Interfaces;
using BookManagement.Web.Services.Models;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Response = Nancy.Response;

namespace BookManagement.Web.Services.Book
{
    public class BookModule : NancyModule
    {
        private readonly IBookRepository _bookRepository;
        public BookModule (IBookRepository bookRepository) : base("/api/book")
        {
            _bookRepository = bookRepository;

            Get("/", _ => GetAllBooks(), null, "GetBooks");
            Post("/",_ => AddBook(), null,"AddBook");
            Get("/{id}", parameters => GetBookById(parameters), null, "GetBookById");
            Delete("/{id}", parameters => DeleteBook(parameters), null, "DeleteBook");
            Put("/{bookId}", parameters => UpdateBook(), null, "UpdateBook");
        }

        private Response UpdateBook()
        {
            var model = this.Bind<BookModel>(); 
            _bookRepository.Update(model);
            return Response.AsJson(model);
        }

        private HttpStatusCode DeleteBook(dynamic parameters)
        {
            _bookRepository.Delete(parameters.Id);
            return HttpStatusCode.OK;
        }

        private Response GetBookById(dynamic parameters)
        {
            var id = (int) parameters.id;
            var response = Response.AsJson(_bookRepository.GetByID(id));
            return response;
        }

        private Negotiator AddBook()
        {
             var model = this.Bind<BookModel>();
             _bookRepository.Add(model);
             return Negotiate.WithModel(model)
             .WithStatusCode(HttpStatusCode.Created);
        }

        private Response GetAllBooks()
        {
            return Response.AsJson(_bookRepository.GetAll());
        }
    }
}
