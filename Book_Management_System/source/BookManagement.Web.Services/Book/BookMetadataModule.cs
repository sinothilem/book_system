﻿using System;
using BookManagement.Web.Services.Models;
using Nancy.Metadata.Modules;
using Nancy.Swagger;
using Swagger.ObjectModel;

namespace BookManagement.Web.Services.Book
{
    public class BookMetadataModule : MetadataModule<PathItem>
    {
        public BookMetadataModule(ISwaggerModelCatalog modelCatalog)
        {
            modelCatalog.AddModels(typeof(BookResponse), typeof(BookResponse));

            Describe["GetBooks"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("GetBooks")
                        .Tag("Book")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Here is the book status"))
                ));

            Describe["GetBookById"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("GetBookById")
                        .Tag("Book")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        .Parameter(new Parameter
                        {
                            Name = "id",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Path,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Book Id",
                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Here is the book status"))
                ));

            Describe["AddBook"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("AddBook")
                        .Tag("Book")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        //specify the parameters of this api   
                        .Parameter(new Parameter
                        {
                            Name = "BookModel",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Body,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Book to be added",
                            Default = new
                            {   
                                title = "The Next Life",
                                author = "Zefaniya",
                                publisher = "SA Home Loans",
                                category = "Education",
                                publishDate = new DateTime(2019,05,03)
                            }

                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Book Added"))
                ));

            Describe["DeleteBook"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("DeleteBook")
                        .Tag("Book")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                        .Parameter(new Parameter
                        {
                            Name = "id",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Path,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Book Id",
                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Book Deleted"))
                ));

            Describe["UpdateBook"] = desc => desc.AsSwagger(
                with => with.Operation(
                    op => op.OperationId("UpdateBook")
                        .Tag("Book")
                        .Summary("Returns a message if the endpoint can be reached")
                        .Description("Returns a message if the endpoint can be reached")
                         //specify the parameters of this api   
                        .Parameter(new Parameter
                        {
                            Name = "bookId",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Path,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Book Id To be updated"
                        })
                        .Parameter(new Parameter
                        {
                            Name = "BookModel",
                            //specify the type of this parameter is path  
                            In = ParameterIn.Body,
                            //specify this parameter is required or not  
                            Required = true,
                            Description = "Book to be updated",
                            Default = new
                            {
                                title = "The Next",
                                authorName = "JOAN DIDION",    
                                publisherName = "Cluster Publications",
                                categoryName = "Music",
                                publishDate = new DateTime(2019, 05, 03)
                            }

                        })
                        .Response(r => r.Schema<HealthResponse>(modelCatalog).Description("Book Updated"))
                ));
        }
    }
}
