﻿using System;

namespace BookManagement.Web.Services.Models
{
    public class BookModel
    {
        public int BookId { get; set; }
        public string Title { get; set; }   
        public string AuthorName { get; set; }
        public string PublisherName { get; set; }
        public string CategoryName { get; set; }
        public DateTime PublishDate { get; set; }
    }
}