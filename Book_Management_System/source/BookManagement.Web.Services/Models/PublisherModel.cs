﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManagement.Web.Services.Models
{
    public class PublisherModel
    {
        public int PublisherId { get; set; }
        public string PublisherName { get; set; }
        public int IsDeleted { get; set; }
    }
}   
