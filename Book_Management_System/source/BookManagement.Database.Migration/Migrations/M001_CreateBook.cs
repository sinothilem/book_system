﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookManagement.Database.Migration.Migrations
{
    [Migration(1)]
    public class M001_CreateBook : FluentMigrator.Migration
    {
        public override void Up()
        {

            Create.Table("Author")
                .WithColumn("AuthorId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("AuthorName").AsString(250).NotNullable()
                .WithColumn("DateOfBirth").AsDateTime().Nullable()
                .WithColumn("IsDeleted").AsInt32().NotNullable();

            Create.Table("Publisher")
                .WithColumn("PublisherId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("PublisherName").AsString(250).NotNullable()
                .WithColumn("IsDeleted").AsInt32().NotNullable();

            Create.Table("Category")
                .WithColumn("CategoryId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("CategoryName").AsString(250).NotNullable()
                .WithColumn("IsDeleted").AsInt32().NotNullable();

            Create.Table("User")
                .WithColumn("UserId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("UserName").AsString(250).NotNullable();

            Create.Table("Book")
                .WithColumn("BookId").AsInt32().PrimaryKey().NotNullable().Identity()
                .WithColumn("Title").AsString(250).NotNullable()
                .WithColumn("AuthorId").AsInt32().ForeignKey("Author", "AuthorId").NotNullable()
                .WithColumn("CategoryId").AsInt32().ForeignKey("Category", "CategoryId").NotNullable()
                .WithColumn("PublisherId").AsInt32().ForeignKey("Publisher", "PublisherId").NotNullable()
                .WithColumn("PublishDate").AsDateTime().Nullable();


            Insert.IntoTable("Publisher").Row(new { PublisherName = "SA Home Loans", IsDeleted = 0})
                                                  .Row(new { PublisherName = "Adapt IT" , IsDeleted = 0 })
                                                  .Row(new { PublisherName = "Derivco" , IsDeleted = 0 })
                                                  .Row(new { PublisherName = "Momentum" , IsDeleted = 0 })
                                                  .Row(new { PublisherName = "SA Home Loans", IsDeleted = 0});

            Insert.IntoTable("Author") .Row(new { AuthorName = "Mark brown" , IsDeleted = 0 })
                                                .Row(new { AuthorName = "Zandile Mbatha" , IsDeleted = 0})
                                                .Row(new { AuthorName = "Lean White" , IsDeleted = 0})
                                                .Row(new { AuthorName = "Lee Moon" , IsDeleted = 0})
                                                .Row(new { AuthorName = "Chris rock" , IsDeleted = 0});

            Insert.IntoTable("Category").Row(new { CategoryName = "Music" , IsDeleted = 0 })
                                                .Row(new { CategoryName = "Cartoons" , IsDeleted = 0 })
                                                .Row(new { CategoryName = "Education" , IsDeleted = 0 })
                                                .Row(new { CategoryName = "Comedy" , IsDeleted = 0 })
                                                .Row(new { CategoryName = "History" , IsDeleted = 0});

        }
        public override void Down()
        {
            Delete.Table("Book");
            Delete.Table("Author");
            Delete.Table("Publisher");
            Delete.Table("Category");
            Delete.Table("User");
        }
    }
}
