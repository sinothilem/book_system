var app = angular.module('MyUserApp', [])
    app.controller('MyUserController', function ($scope, $http, $window) {
        
        $scope.hasBooks = false;
        $scope.categories;
        $scope.showTab = "cat";

        $scope.GetBooks = function () {   
        var post = $http({  
                method: "GET",
                url: "http://localhost:5050/book/",
                dataType: 'json',
                headers: { 
                    "Content-Type": "application/json" }
            })
            post.success(function (data) {
                $scope.books = data;
               
            }) 
        //.then(function(data) {
        //         $scope.catId = $state.params.categoryId;
        //         $scope.data = data;
        //            for (var i= 0; i < $scope.data.length; i++){
        //              if ($scope.data[i].categoryId == $stateParams.categoryId){
        //                   $scope.data = data; 
        //                  console.log($scope.data[i].product_name);
        //              }  
        //            } return null;  
        //    });

            post.error(function (data) {
                $window.alert(data.Message);
            });
        }

        $scope.ListOfCategories = function () {   
            var post = $http({
                    method: "GET",
                    url: "http://localhost:5050/book/categories",
                    dataType: 'json',
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (data) {
                    $scope.categories = data.data;
                    var select = document.getElementById("category");
                    for(var i = 0; i < data.data.length; i++) {
                        var opt = data.data[i].categoryName;
                        var el = document.createElement("option");
                        el.textContent = opt;
                        el.value = opt;
                        select.appendChild(el);
                   }
                   $scope.GetBooks();
                });
            };

            $scope.ListOfCategories();
            $scope.GetBooks();
                       
            // $scope.openPage = function(pageName,elmnt,color) {
            //     var i, tabcontent, tablinks;
            //     tabcontent = document.getElementsByClassName("tabcontent");
            //     for (i = 0; i < tabcontent.length; i++) {                     
            //       tabcontent[i].style.display = "none";
            //     }
            //     tablinks = document.getElementsByClassName("tablink");
            //     for (i = 0; i < tablinks.length; i++) {
            //       tablinks[i].style.backgroundColor = "";
            //     }
            //     document.getElementById(pageName).style.display = "block";
            //     elmnt.style.backgroundColor = color;
                
            //   }
              
              // Get the element with id="defaultOpen" and click on it
              

            // $scope.categories = Object.keys($scope.categories.reduce(function(categoryMap, product) {
            //     categoryMap[product.category] = 1;
            //     return categoryMap;
            // }, {}));
            
            $scope.selectCategory = function(category) {

                $scope.selectedCategoryProjects = [];
                $scope.books.forEach(function(element){
                 if(element.categoryName == category.categoryName){
                    $scope.selectedCategoryProjects.push(element);
                    }
                });
               // document.getElementById("cooking").click();
            }

            
            //  for( y = 0; y <= $scope.categories.length; y++){
            //     var idName = document.getElementById($scope.categories.categoryName[y]);
            //     $scope.booksInCategory = [];
            //     if(idName ===  $scope.books.array.forEach(element => {
            //         element.categoryName
            //     })){
            //        $scope.booksInCategory.push(element);
            //         $scope.hasBooks = true;
            //     };

            //  }
       

});