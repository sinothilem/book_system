var app = angular.module('MyApp', [])

    app.controller('MyController', function ($scope, $http, $window, $element) {
        
        $scope.isDisabled = false;

          $scope.submit = function() {
            if (!$scope.username && !$scope.password) {
              } 
    
            else if ($scope.username === "admin" && $scope.password === "123") {
              window.location = "\Admin.html"
        
            } 
            else if ($scope.username === "sno" && $scope.password === "123") {
                window.location = "\Users.html"
          
              }else {
              //alert("Invalid Login");
              toastr.error("The Username or password you entered is incorrect");
            }
          }

        $scope.GetBooks = function () {   
        var post = $http({
                method: "GET",
                url: "http://localhost:5050/book/",
                dataType: 'json',
                headers: { 
                    "Content-Type": "application/json" }
            })
            post.success(function (data) {
                $scope.books = data;
            });

            post.error(function (data) {
                $window.alert(data.Message);
            });
        }
        
        $scope.AddBooks = function () {

            let book = 
            {title: $scope.title,
            categoryName: $scope.category,
            authorName: $scope.author,
            publisherName: $scope.publisher,
            publishDate: new Date ($scope.publishDate)};

            if(!$scope.title || !$scope.category || !$scope.author || !$scope.publisher || !$scope.publishDate){
                toastr.error("Please Enter Book Information");
              }

            else{
                var post = $http({
                    method: "POST",
                    url: "http://localhost:5050/api/book/",
                    dataType: 'json',
                    data: book,
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (response) {
                    toastr.success("Successfully Added");
                    $scope.repos = response.data;
                    $scope.GetBooks();
                    $scope.id = null;
                    $scope.title = null;
                    $scope.category = null;
                    $scope.author = null;
                    $scope.publisher = null;
                    $scope.publishDate = null;
                });
             }
        };

        $scope.DeleteBooks = function () {

            let book = {id:$scope.id};
            if(!$scope.id){
                toastr.error("Please select Book To Delete");
            }
            else{
                
            $scope.showconfirmbox();
            
            if($scope.result == "Yes"){
            
 
             var post = $http({
                 method: "DELETE",
                 url: "http://localhost:5050/api/book/"+ $scope.id,
                 dataType: 'json',
                 data: book,
                 headers: { 
                     "Content-Type": "application/json" }
             }).finally(function (response) {
                 $scope.GetBooks();
                 toastr.success("Successfully Deleted");
                 $scope.id = null;
                 $scope.title = null;
                 $scope.category = null;
                 $scope.author = null;
                 $scope.publisher = null;
                 $scope.publishDate = null;
 
                 $scope.isDisabled = false;
              });
            }

            }  
        };

        $scope.UpdateBooks = function () {
            
            let book = 
            {
            id:$scope.id,
            title: $scope.title,
            categoryName: $scope.category,
            authorName: $scope.author,
            publisherName: $scope.publisher,
            publishDate: new Date($scope.publishDate)
          };

          if(!$scope.id){
            toastr.error("Please select Book To Update");
          }

          else{
            var post = $http({
                method: "PUT",
                url: "http://localhost:5050/api/book/"+ book.id ,
                dataType: 'json',
                data: book,
                headers: { 
                    "Content-Type": "application/json" }
            })
            .then(function (response) {
                toastr.success("Successfully Updated");
                $scope.repos = response.data;
                $scope.GetBooks();
                $scope.id = null;
                $scope.title = null;
                $scope.category = null;
                $scope.author = null;
                $scope.publisher = null;
                $scope.publishDate = null;

                $scope.isDisabled = false;
            });
          }
        };

        // $scope.ListOfAuthors = function () {   
        //     var post = $http({
        //             method: "GET",
        //             url: "http://localhost:5050/api/author",
        //             dataType: 'json',
        //             headers: { 
        //                 "Content-Type": "application/json" }
        //         })
        //         .then(function (data) {
        //             $scope.authors = data;
        //             var select = document.getElementById("author");
        //             for(var i = 0; i < data.data.length; i++) {
        //                 var opt = data.data[i].authorName;
        //                 var el = document.createElement("option");
        //                 el.textContent = opt;
        //                 el.value = opt;
        //                 select.appendChild(el);
        //            }
        //            $scope.GetBooks();
        //         });
        //     };

            // $scope.Authors = function () {   
            //     var post = $http({
            //             method: "GET",
            //             url: "http://localhost:5050/book/authors",
            //             dataType: 'json',
            //             headers: { 
            //                 "Content-Type": "application/json" }
            //         })
            //         .then(function (data) {
            //             $scope.a = data;
            //         });
            //     };

        $scope.ListOfCategories = function () {   
            var post = $http({
                    method: "GET",
                    url: "http://localhost:5050/book/categories",
                    dataType: 'json',
                    headers: { 
                        "Content-Type": "application/json" }
                })
                .then(function (data) {
                    $scope.categories = data.data;
                    var select = document.getElementById("category");
                    for(var i = 0; i < data.data.length; i++) {
                        var opt = data.data[i].categoryName;
                        var el = document.createElement("option");
                        el.textContent = opt;
                        el.value = opt;
                        select.appendChild(el);
                   }
                   $scope.GetBooks();
                });
            };

            $scope.ListOfPublishers = function () {   
                var post = $http({
                        method: "GET",
                        url: "http://localhost:5050/book/publishers",
                        dataType: 'json',
                        headers: { 
                            "Content-Type": "application/json" }
                    })
                    .then(function (data) {
                        $scope.books = data;
                        var select = document.getElementById("publisher");
                        for(var i = 0; i < data.data.length; i++) {
                            var opt = data.data[i].publisherName;
                            var el = document.createElement("option");
                            el.textContent = opt;
                            el.value = opt;
                            select.appendChild(el);
                       }
                       $scope.GetBooks();
                    });
                };









                $scope.GetAuthors = function () {   
                    var post = $http({
                            method: "GET",
                            url: "http://localhost:5050/api/author",
                            dataType: 'json',
                            headers: { 
                                "Content-Type": "application/json" }
                        })
                        .then(function (data) {
                          $scope.authors = data.data;
                          var select = document.getElementById("author");
                          for(var i = 0; i < data.data.length; i++) {
                              var opt = data.data[i].authorName;
                              var el = document.createElement("option");
                              el.textContent = opt;
                              el.value = opt;
                              select.appendChild(el);
                         }
                         $scope.GetBooks();
                          
                        });
                    };
        
                    $scope.GetCategories = function () {   
                        var post = $http({
                                method: "GET",
                                url: "http://localhost:5050/book/categories",
                                dataType: 'json',
                                headers: { 
                                    "Content-Type": "application/json" }
                            })
                            .then(function (data) {
                              $scope.categories = data.data;
                            });
                        };
        
                        $scope.GetPublishers = function () {   
                            var post = $http({
                                    method: "GET",
                                    url: "http://localhost:5050/book/publishers",
                                    dataType: 'json',
                                    headers: { 
                                        "Content-Type": "application/json" }
                                })
                                .then(function (data) {
                                  $scope.publishers = data.data;
                                });
                            };
        
                    $scope.AddAuthors = function () {
        
                        let author = 
                        { 
                            authorName: $scope.authorName,
                            dateOfBirth: new Date($scope.birthDate)
                        };
                        
                        if(!$scope.authorName){
                            toastr.error("Please Enter Author Name");
                          }
                          else{
                            var post = $http({
                                method: "POST",
                                url: "http://localhost:5050/api/author/",
                                dataType: 'json',
                                data: author,
                                headers: { 
                                    "Content-Type": "application/json" }
                            })
                            .then(function (response) {
                                toastr.success("Successfully Added");
                                $scope.repos = response.data;
                                $scope.GetAuthors();
                                $scope.authorName = null;
                                $scope.birthDate = null;
                            });
                          }
                    };
        
                    $scope.AddCategories = function () {
        
                        let categories = 
                        { 
                            categoryName: $scope.categoryName,
                        };

                        if(!$scope.categoryName){
                            toastr.error("Please Enter Category Name");
                          }
                          else{
                            var post = $http({
                                method: "POST",
                                url: "http://localhost:5050/api/category/",
                                dataType: 'json',
                                data: categories,
                                headers: { 
                                    "Content-Type": "application/json" }
                            })
                            .then(function (response) {
                                toastr.success("Successfully Added");
                                $scope.repos = response.data;
                                $scope.GetCategories();
                                $scope.categoryId = null;
                                $scope.categoryName = null;
                            });
                          }
            
                       
                    };
        
                    $scope.AddPublishers = function () {
        
                        let publisher = 
                        { 
                            publisherName: $scope.publisherName,
                        };

                        if(!$scope.publisherName){
                            toastr.error("Please Enter publisher Name");
                          }
                          else{
                            var post = $http({
                                method: "POST",
                                url: "http://localhost:5050/api/publisher/",
                                dataType: 'json',
                                data: publisher,
                                headers: { 
                                    "Content-Type": "application/json" }
                            })
                            .then(function (response) {
                                toastr.success("Successfully Added");
                                $scope.repos = response.data;
                                $scope.GetPublishers();
                                $scope.publisherName = null;
                            });

                          }     
                    };
        
        
                    $scope.UpdateAuthors = function () {
        
                        let author = 
                        { 
                            authorId: $scope.authorId,
                            authorName: $scope.authorName,
                            dateOfBirth: new Date($scope.birthDate)
                        };

                        if(!$scope.authorId){
                            toastr.error("Please select Author To Update");
                          }
                       else{
                        var post = $http({
                            method: "PUT",
                            url: "http://localhost:5050/api/author/" + author.authorId,
                            dataType: 'json',
                            data: author,
                            headers: { 
                                "Content-Type": "application/json" }
                        })
                        .then(function (response) {
                            toastr.success("Successfully Updated");
                            $scope.repos = response.data;
                            $scope.GetAuthors();
                            $scope.authorId = null;
                            $scope.authorName = null;
                            $scope.birthDate = null;
                        });

                       }
                    };
        
                    $scope.UpdateCategories = function () {
        
                        let category= 
                        { 
                            categoryId: $scope.categoryId,
                            categoryName: $scope.categoryName,
                        };
                        if(!$scope.categoryId){
                            toastr.error("Please select Category To Update");
                          }
                          else{
                            var post = $http({
                                method: "PUT",
                                url: "http://localhost:5050/api/category/" + category.categoryId,
                                dataType: 'json',
                                data: category,
                                headers: { 
                                    "Content-Type": "application/json" }
                            })
                            .then(function (response) {
                                toastr.success("Successfully Updated");
                                $scope.repos = response.data;
                                $scope.GetCategories();
                                $scope.categoryId = null;
                                $scope.categoryName = null;
                            });

                          }
                    };

                    $scope.UpdatePublishers = function () {
        
                        let publisher= 
                        { 
                            publisherId: $scope.publisherId,
                            publisherName: $scope.publisherName,
                        };
                        if(!$scope.publisherId){
                            toastr.error("Please select Publisher To Update");
                          }
                          else{
                            var post = $http({
                                method: "PUT",
                                url: "http://localhost:5050/api/publisher/" + publisher.publisherId,
                                dataType: 'json',
                                data: publisher,
                                headers: { 
                                    "Content-Type": "application/json" }
                            })
                            .then(function (response) {
                                toastr.success("Successfully Updated");
                                $scope.repos = response.data;
                                $scope.GetPublishers();
                                $scope.publisherId = null;
                                $scope.publisherName = null;
                            });

                          }
                    };
        
                    $scope.DeleteAuthors = function () {

                        let author = {authorId: $scope.authorId};

                        if(!$scope.authorId){
                            toastr.error("Please select Author To Delete");
                        }
                        else{
                            $scope.showconfirmbox();
                        
                            if($scope.result == "Yes"){
                            
                 
                             var post = $http({
                                 method: "DELETE",
                                 url: "http://localhost:5050/api/author/"+ author.authorId,
                                 dataType: 'json',
                                 data: author,
                                 headers: { 
                                     "Content-Type": "application/json" }
                             }).finally(function (response) {
                                 $scope.GetAuthors();
                                 toastr.success("Successfully Deleted");
                                 $scope.id = null;
                                 $scope.title = null;
                                 $scope.category = null;
                                 $scope.author = null;
                                 $scope.publisher = null;
                                 $scope.publishDate = null;
                 
                                 $scope.isDisabled = false;
                              });
                            }
                        }
                    };
        
                    
                    $scope.DeleteCategories = function () {

                        let categories = {categoryId: $scope.categoryId};

                        if(!$scope.categoryId){
                            toastr.error("Please select Category To Delete");
                        }
                        else{
                            $scope.showconfirmbox();
                        
                       if($scope.result == "Yes"){
                        
            
                        var post = $http({
                            method: "DELETE",
                            url: "http://localhost:5050/API/category/"+ categories.categoryId,
                            dataType: 'json',
                            data: categories,
                            headers: { 
                                "Content-Type": "application/json" }
                        }).finally(function (response) {
                            $scope.GetCategories();
                            toastr.success("Successfully Deleted");
                            $scope.categoryId = null;
                            $scope.categoryName = null;
            
                            $scope.isDisabled = false;
                         });
                       }

                      } 
                        
                    };

                    $scope.DeletePublishers = function () {

                        let publisher = {publisherId: $scope.publisherId};

                        if(!$scope.publisherId){
                            toastr.error("Please select Publisher To Delete");
                        }
                        else{
                            $scope.showconfirmbox();
                        
                            if($scope.result == "Yes"){              
                 
                             var post = $http({
                                 method: "DELETE",
                                 url: "http://localhost:5050/api/publisher/"+ publisher.publisherId,
                                 dataType: 'json',
                                 data: publisher,
                                 headers: { 
                                     "Content-Type": "application/json" }
                             }).finally(function (response) {
                                 $scope.GetPublishers();
                                 toastr.success("Successfully Deleted");
                                 $scope.publisherId = null;
                                 $scope.publisherName = null;
                 
                                 $scope.isDisabled = false;
                              });
                            }
                        }
                    };
        
                    $scope.selectBook = function(e) {
                        var table = document.getElementById('tableBook'),rIndex;
                        for(var i = 0; i < table.rows.length; i++){
                            table.rows[i].onclick = function(){
                                rIndex = this.rowIndex;
                                $scope.id = this.cells[0].innerHTML;
                                $scope.title = this.cells[1].innerHTML;
                                $scope.category = this.cells[2].innerHTML;
                                $scope.author = this.cells[3].innerHTML;
                                $scope.publisher = this.cells[4].innerHTML;
                                $scope.publishDate = this.cells[5].innerHTML;
                            }
                        }
                      }
    

                $scope.selectAuthor = function(e) {
                    var table = document.getElementById('tableAuthor'),rIndex;
                    for(var i = 0; i < table.rows.length; i++){
                        table.rows[i].onclick = function(){
                            rIndex = this.rowIndex;
                            $scope.authorId = this.cells[0].innerHTML;
                            $scope.authorName = this.cells[1].innerHTML;
                            $scope.birthDate = this.cells[2].innerHTML;
                        }
                    }
                  }

                  
                $scope.selectCategories = function(e) {
                    var table = document.getElementById('tableCategories'),rIndex;
                    for(var i = 0; i < table.rows.length; i++){
                        table.rows[i].onclick = function(){
                            rIndex = this.rowIndex;
                            $scope.categoryId = this.cells[0].innerHTML;
                            $scope.categoryName = this.cells[1].innerHTML;
                        }
                    }
                  }

                  $scope.selectPublishers = function(e) {
                    var table = document.getElementById('tablePublishers'),rIndex;
                    for(var i = 0; i < table.rows.length; i++){
                        table.rows[i].onclick = function(){
                            rIndex = this.rowIndex;
                            $scope.publisherId = this.cells[0].innerHTML;
                            $scope.publisherName = this.cells[1].innerHTML;
                        }
                    }
                  }
        
    

     $scope.showconfirmbox = function () {
        if ($window.confirm("Are you Sure?"))
        $scope.result = "Yes";
        else
        $scope.result = "No";
        }
    
        $scope.GetBooks();
        $scope.ListOfCategories();
        $scope.ListOfPublishers();
        //$scope.ListOfAuthors();
       // $scope.Authors();

       $scope.GetCategories();
       $scope.GetAuthors();
       $scope.GetPublishers();
});
