CREATE TABLE `BookManagement`.`Book` (
  `BookId` INT NOT NULL AUTO_INCREMENT,
  `Title` VARCHAR(45) NOT NULL,
  `AuthorId` INT NOT NULL,
  `CategoryId` INT NOT NULL,
  `PublisherId` INT NOT NULL,
  `PublishDate` DATETIME NULL,
  PRIMARY KEY (`BookId`));



  CREATE TABLE `BookManagement`.`Author` (
  `AuthorId` INT NOT NULL AUTO_INCREMENT,
  `AuthorName` VARCHAR(45) NOT NULL,
  `DateOfBirth` DATETIME NULL,
  `IsDeleted` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`AuthorId`));



CREATE TABLE `BookManagement`.`Publisher` (
  `PublisherId` INT NOT NULL AUTO_INCREMENT,
  `PublisherName` VARCHAR(45) NOT NULL,
  `IsDeleted` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`PublisherId`));



CREATE TABLE `BookManagement`.`Category` (
  `CategoryId` INT NOT NULL AUTO_INCREMENT,
  `CategoryName` VARCHAR(45) NOT NULL,
  `IsDeleted` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`CategoryId`));


CREATE TABLE `BookManagement`.`User` (
  `UserId` INT NOT NULL AUTO_INCREMENT,
  `UserName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`UserId`));


insert into Category (CategoryName,IsDeleted) values ("Health And Fitness",0);
insert into Category (CategoryName,IsDeleted) values ("Cooking",0);
insert into Category (CategoryName,IsDeleted) values ("Entertainment",0);
insert into Category (CategoryName,IsDeleted) values ("Medical",0);
insert into Category (CategoryName,IsDeleted) values ("Sport",0);
insert into Category (CategoryName,IsDeleted) values ("Music",0);
insert into Category (CategoryName,IsDeleted) values ("Kids",0);


insert into Publisher (PublisherName,IsDeleted) values('Reach Publishers',0);
insert into Publisher (PublisherName,IsDeleted) values('Breeze Publishing',0);
insert into Publisher (PublisherName,IsDeleted) values('Trumpeter Publishing',0);
insert into Publisher (PublisherName,IsDeleted) values('Cluster Publications',0);
insert into Publisher (PublisherName,IsDeleted) values('Pan Macmillan South Africa',0);
insert into Publisher (PublisherName,IsDeleted) values('Umsinsi Press CC',0);



insert into Author (AuthorName,DateOfBirth,IsDeleted) values ('ERNEST HEMINGWAY','1980-12-18',0);
insert into Author (AuthorName,DateOfBirth,IsDeleted) values ('JOAN DIDION','1990-01-05',0);
insert into Author (AuthorName,DateOfBirth,IsDeleted) values ('GEORGE R.R. MARTIN','1960-05-19',0);
insert into Author (AuthorName,DateOfBirth,IsDeleted) values ('RAY BRADBURY','1976-11-20',0);
insert into Author (AuthorName,DateOfBirth,IsDeleted) values ('AYN RAND','1966-02-24',0);
insert into Author (AuthorName,DateOfBirth,IsDeleted) values ('GILLIAN FLYNN','1980-04-02',0);
insert into Author (AuthorName,DateOfBirth,IsDeleted) values ('JANE AUSTEN','1985-07-04',0);