﻿using System;
using Nancy.Hosting.Self;

namespace BookManagement.Web    
{
    public class Program
    {
        static void Main(string[] args)
        {
            var hostConfigs = new HostConfiguration
            {
                UrlReservations =
                {
                    CreateAutomatically = true
                }
            };

            var uri = new Uri("http://localhost:5050");
            using (var host = new NancyHost( hostConfigs, uri))
            {
                host.Start();
                Console.ReadKey();
            }

        }

    }
}
